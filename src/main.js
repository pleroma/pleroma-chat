import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import { Socket } from 'phoenix'

Vue.use(Vuex)

Vue.config.productionTip = false

let utoa = (str) => {
  // first we use encodeURIComponent to get percent-encoded UTF-8,
  // then we convert the percent encodings into raw bytes which
  // can be fed into btoa.
  return btoa(encodeURIComponent(str)
    .replace(/%([0-9A-F]{2})/g,
      (match, p1) => { return String.fromCharCode('0x' + p1) }))
}

const authHeaders = (user) => {
  return { 'Authorization': `Basic ${utoa(`${user.username}:${user.password}`)}` }
}

const store = new Vuex.Store({
  state: {
    user: null,
    socket: null,
    currentRoom: null,
    users: {},
    rooms: {
      public: {
        messages: [],
        channel: null
      }
    }
  },
  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setSocket (state, socket) {
      state.socket = socket
    },
    setMessages (state, {room, messages}) {
      console.log(messages)
      messages = messages.slice(-19, 20)
      state.rooms[room].messages = messages
    },
    addMessage (state, {room, message}) {
      state.rooms[room].messages.push(message)
    },
    setChannel (state, {room, channel}) {
      state.rooms[room].channel = channel
    },
    setCurrentRoom (state, room) {
      state.currentRoom = room
    }
  },
  actions: {
    joinRoom (context, roomName) {
      const socket = context.state.socket
      const channel = socket.channel(`chat:${roomName}`)
      channel.on('new_msg', (msg) => {
        context.commit('addMessage', {room: roomName, message: msg})
      })
      channel.on('messages', ({messages}) => {
        context.commit('setMessages', {room: roomName, messages: messages})
      })
      channel.join()
      context.commit('setChannel', {room: roomName, channel: channel})
      context.commit('setCurrentRoom', roomName)
    },
    login (context, user) {
      const LOGIN_URL = '/api/account/verify_credentials.json'
      const headers = authHeaders(user)
      console.log(headers)
      return window.fetch(LOGIN_URL, {
        method: 'POST',
        headers
      }).then((res) => res.json())
        .then((user) => {
          context.commit('setUser', user)
          let socket = new Socket('/socket', {params: {token: user.token}})
          socket.connect()
          context.commit('setSocket', socket)
          context.dispatch('joinRoom', 'public')
          router.push('/')
        })
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
